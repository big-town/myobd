#!/usr/bin/python
# -*- coding: utf-8 -*-
from __future__ import division

# импортирование модулей python
from Tkinter import *
import threading
import time
import random
import SetForm
import config as cfg
import os.path 
import obd

def getRPM():
	cmd = obd.commands.RPM # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	   	return int(response.value)
	return 0

def getTHROTTLE():
	cmd = obd.commands.THROTTLE_POS # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	   	return int(response.value)
	return 0

def getSPEED():
	cmd = obd.commands.SPEED # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    return int(response.value)
	return 0

def GraphInit():
	#global cfg.RPM, cfg.line1, cfg.Scale, cfg.line2, cfg.fline1, cfg.fline2, cfg.Tick, cfg.GraphX, cfg.GraphY, cfg.dGraphY, cfg.COUNT_ELEM, cfg.COUNT_ELEM2
	cfg.canv.delete('all')
	cfg.PosText=cfg.canv.create_text(90,30,text="00",fill="red",font="Verdana 24")
	
	cfg.RPM=0
	cfg.line1=[0]
	cfg.Scale=[0]
	cfg.line2=[0]
	cfg.fline1=[]
	cfg.fline2=[]
	cfg.Tick=0
	cfg.GraphX=[0]
	cfg.GraphY=[0]
	cfg.fGraphY=[]
	cfg.dGraphY=[0]
	cfg.COUNT_ELEM=0
	cfg.COUNT_ELEM2=0
	cfg.canv.create_line(10,10,10,310,width=3,fill="yellow",arrow=FIRST)
	cfg.canv.create_line(10,310,470,310,width=3,fill="yellow",arrow=LAST)

	for y in xrange(10,310,50):
		cfg.canv.create_line(10,y,470,y,width=1,fill="green")
	cfg.Scale.append(cfg.canv.create_line(470,10,470,310,width=1,fill="green"))
	

def GetValue():
	TimeOut=0.1
	StartMessure=0
	TimeMessure=[]
	ThrottleMessure=[]
	RpmMessure=[]


	while cfg.RUNING: 
		#Get RPM Speed
		CurrentSpeed=getSPEED()
		CurrentRPM=getRPM()
		CurrentPOS=getTHROTTLE()
		#print CurrentPOS
		#cfg.PosText.configure(text=str(CurrentPOS))
		cfg.canv.itemconfig(cfg.PosText,text=str(CurrentPOS))
		if (StartMessure==0 and CurrentPOS>=cfg.MAX_THROTTLE_VALUE) and (( not cfg.START_SPEED_VALUE) or (CurrentSpeed>=cfg.START_SPEED_VALUE)) and (CurrentRPM>=cfg.MIN_RPM_VALUE):
			TimeBegin=time.time()
			DriveBegin=time.strftime("%X")
			StartMessure=1
			#cfg.PosText.configure(text=str(CurrentPOS))
			while StartMessure:
				CurrentSpeed=getSPEED()
				CurrentRPM=getRPM()
				CurrentPOS=getTHROTTLE()

#				if time.time()-TimeBegin>=TimeOut:
				TimeBegin=time.time()
				TimeMessure.append(TimeBegin)
				ThrottleMessure.append(CurrentPOS)
				RpmMessure.append(CurrentRPM)
				cfg.canv.itemconfig(cfg.PosText,text=str(CurrentPOS))

				if StartMessure==1 and CurrentPOS<=cfg.MIN_THROTTLE_VALUE:
					StartMessure=0
					GraphInit()
					f=open(time.strftime("%d-%m-%Y_%X")+"_"+DriveBegin+"_"+str(CurrentSpeed)+".csv","w")
					f.write("RPM,Time,TPS\n");
					for i in range(0,len(ThrottleMessure)):
						cfg.RPM=RpmMessure[i]
						PaintGraph()
						f.write(str(RpmMessure[i])+","+str(TimeMessure[i])+","+str(ThrottleMessure[i])+"\n")
					f.close()
					TimeMessure=[]
					ThrottleMessure=[]
					RpmMessure=[]
					#time.sleep(0.2)
		time.sleep(0.1)

#def myButtonClick(event):
#		PaintGraph()

def CallSetForm(event):
	SetForm.CreateSetForm(root)
		

def PaintCompGraph():
	#Если количество элементов в сравниваемом файле больше чем в текущем графике 
	if (cfg.COUNT_ELEM2-1)>cfg.COUNT_ELEM:
		#Передвигаем линии координатной сетки на новые координаты то что уже есть
		for elem in range(0,cfg.COUNT_ELEM+1):
			print elem
			cfg.GraphX[elem]=int(10+(float(460)/(cfg.COUNT_ELEM2-1))*elem)
			#cfg.GraphX[elem]=int(10+(float(460)/(cfg.COUNT_ELEM+1))*elem)
			X2_O=int(cfg.GraphX[elem])
			cfg.canv.coords(cfg.Scale[elem],X2_O,10,X2_O,310)
		#Добавляем новые линии в координатной сетке и заполняем массив cfg.GraphX	
		for elem in range(cfg.COUNT_ELEM+1,cfg.COUNT_ELEM2):
			cfg.GraphX.append(int(10+(float(460)/(cfg.COUNT_ELEM2-1))*elem))
			X2_O=int(cfg.GraphX[elem])
			cfg.Scale.append(cfg.canv.create_line(X2_O,10,X2_O,310,width=1,fill="green"))
		#Передвигаем существующие графики по новым координатам содержащиееся в массиве cfg.GraphX
		for elem in range(1,cfg.COUNT_ELEM+1):
			X1_O=int(cfg.GraphX[elem-1])
			Y1_O=310-int(cfg.GraphY[elem-1])*0.05
			X2_O=int(cfg.GraphX[elem])
			Y2_O=310-int(cfg.GraphY[elem])*0.05
#			print elem,"*",len(cfg.dGraphY)
			dY1_O=cfg.dRelative-int(cfg.dGraphY[elem-1])*cfg.dStrenght
			dY2_O=cfg.dRelative-int(cfg.dGraphY[elem])*cfg.dStrenght
			cfg.canv.coords(cfg.line1[elem],X1_O,Y1_O,X2_O,Y2_O)
			cfg.canv.coords(cfg.line2[elem],X1_O,dY1_O,X2_O,dY2_O)

	
	for elem in range(0,cfg.COUNT_ELEM2-1):
		X1_O=int(cfg.GraphX[elem])
		Y1_O=310-int(cfg.fGraphY[elem])*0.05
		X2_O=int(cfg.GraphX[elem+1])
		Y2_O=310-int(cfg.fGraphY[elem+1])*0.05
		cfg.fline1.append(cfg.canv.create_line(X1_O,Y1_O,X2_O,Y2_O,width=1,fill="blue"))


	for elem in range(1,cfg.COUNT_ELEM2-1):
		X1_O=int(cfg.GraphX[elem])
		dY1_O=cfg.dRelative-int((int(cfg.fGraphY[elem])-int(cfg.fGraphY[elem-1]))*cfg.dStrenght)
		X2_O=int(cfg.GraphX[elem+1])
		dY2_O=cfg.dRelative-int((int(cfg.fGraphY[elem+1])-int(cfg.fGraphY[elem]))*cfg.dStrenght)
		cfg.fline1.append(cfg.canv.create_line(X1_O,dY1_O,X2_O,dY2_O,width=1,fill="cyan"))

def PaintGraph():
	if cfg.Tick==0:
		cfg.GraphX[0]=10
		cfg.dGraphY[0]=0
		cfg.GraphY[0]=cfg.RPM
	
	if cfg.Tick==1:
		cfg.GraphY.append(cfg.RPM)
		cfg.GraphX.append(470)
		cfg.dGraphY.append(cfg.RPM-cfg.GraphY[0])
		Y1_O=310-int(cfg.GraphY[0]*0.05)
		dY1_O=cfg.dRelative-int(cfg.dGraphY[0]*cfg.dStrenght)
		Y2_O=310-int(cfg.GraphY[1]*0.05)
		dY2_O=cfg.dRelative-int(cfg.dGraphY[1]*cfg.dStrenght)

		cfg.line1.append(cfg.canv.create_line(10,Y1_O,470,Y2_O,width=1,fill="red"))
		cfg.line2.append(cfg.canv.create_line(10,dY1_O,470,dY2_O,width=1,fill="magenta"))
		cfg.COUNT_ELEM=1

	if cfg.Tick>=2:
		for elem in range(1,cfg.COUNT_ELEM+1):
			cfg.GraphX[elem]=int(10+(float(460)/(cfg.COUNT_ELEM+1))*elem)
			X1_O=int(cfg.GraphX[elem-1])
			Y1_O=310-int(cfg.GraphY[elem-1]*0.05)
			X2_O=int(cfg.GraphX[elem])
			Y2_O=310-int(cfg.GraphY[elem]*0.05)

			dY1_O=cfg.dRelative-int(cfg.dGraphY[elem-1]*cfg.dStrenght)
			dY2_O=cfg.dRelative-int(cfg.dGraphY[elem]*cfg.dStrenght)
			cfg.canv.coords(cfg.Scale[elem],X2_O,10,X2_O,310)
			cfg.canv.coords(cfg.line1[elem],X1_O,Y1_O,X2_O,Y2_O)
			cfg.canv.coords(cfg.line2[elem],X1_O,dY1_O,X2_O,dY2_O)

		cfg.dGraphY.append(cfg.RPM-cfg.GraphY[cfg.Tick-1])
		cfg.GraphY.append(cfg.RPM)
		cfg.GraphX.append(470)
		Y3_O=310-int(cfg.GraphY[cfg.Tick]*0.05)
		dY3_O=cfg.dRelative-int(cfg.dGraphY[cfg.Tick]*0.05)

		cfg.Scale.append(cfg.canv.create_line(470,10,470,310,width=1,fill="green"))
		cfg.line1.append(cfg.canv.create_line(X2_O,Y2_O,470,Y3_O,width=1,fill="red"))
		cfg.line2.append(cfg.canv.create_line(X2_O,dY2_O,470,dY3_O,width=1,fill="magenta"))
		cfg.COUNT_ELEM+=1
	cfg.Tick+=1
	#print "--------------------"
def wmDestroy(event):
	cfg.RUNING=0

if  __name__ ==  "__main__" :


	root=Tk()
	connection = obd.OBD("/dev/ttyUSB0")

	root.geometry("480x320")
	root.attributes('-fullscreen', True)
	if os.path.isfile("settings.ini") and os.path.exists("settings.ini"):
		f=open("settings.ini","r")
		cfg.MIN_THROTTLE_VALUE=int(f.readline())
		cfg.MAX_THROTTLE_VALUE=int(f.readline())
		cfg.START_SPEED_VALUE=int(f.readline())
		cfg.MIN_RPM_VALUE=int(f.readline())
		f.close()

	cfg.canv = Canvas(root,width=480,height=320,bg="black")
	#myButton=Button(cfg.canv,text=">>")
	#myButton.bind("<Button-1>",myButtonClick)

	SetButton=Button(cfg.canv,text="Set")
	SetButton.bind("<Button-1>",CallSetForm)
	
	cfg.canv.place(x=0,y=0)
	#PosText.place(x=100,y=30)
	#myButton.place(x=440,y=0)
	SetButton.place(x=15,y=15)
	GraphInit()
	

	#root.bind('<Destroy>',wmDestroy)
	
	Thread_GetValue=threading.Thread(target=GetValue)
	Thread_GetValue.daemon = False
	Thread_GetValue.start()	

	root.mainloop()