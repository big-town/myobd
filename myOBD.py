#!/usr/bin/python
from __future__ import division
# -*- coding: utf-8 -*-

from PIL import Image, ImageDraw, ImageTk, ImageFont
import Tkinter as tk
import threading
import time
import sys
import obd
#global gif,rotated,ImageTk,image,tk_img, my_count, new_img, pen

#my_count=1

STOP_ACCEL=1
STOP_PRINT=1
STOP_GET=1
"""
GET_RPM=3800
GET_SPEED=120
GET_SPEED_EXACTLY=0
GET_LOAD=100
GET_LOAD_ABS=100
GET_THROTTLE=100
GET_THROTTLE_ABS=100
GET_SHORT_L=1.6
GET_LONG_L=2.3
GET_TIMING=17.5
GET_TEMP=92
GET_FUEL_STATUS="Open loop acceleration"
GET_ACCEL=0
"""
GET_RPM="0"
GET_SPEED="0"
GET_SPEED_EXACTLY=0
GET_LOAD="0"
GET_LOAD_ABS="0"
GET_THROTTLE="0"
GET_THROTTLE_ABS="0"
GET_SHORT_L="0"
GET_LONG_L="0"
GET_TIMING="0"
GET_TEMP="0"
GET_FUEL_STATUS=" "
GET_FUEL_LEVEL="0"
GET_AFR="0"
GET_ACCEL=0

ACCEL_X=320

def str_int(str,digits=0):
    index=str.find(".")
#    print index
    if index==-1:
	return str
    if index>0:
	if digits>0:
	    return str[0:index+digits+1]
	return str[0:index]
    return "0"

def getValues():
    global GET_AFR,GET_FUEL_LEVEL,GET_SPEED_EXACTLY,GET_FUEL_STATUS,GET_TEMP,GET_RPM,GET_SPEED,GET_LOAD,GET_LOAD_ABS,GET_THROTTLE,GET_THROTTLE_ABS,GET_SHORT_L,GET_LONG_L,GET_TIMING
    while STOP_GET:
#1
	cmd = obd.commands.RPM # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_RPM=str_int(str(response.value))

	cmd = obd.commands.SPEED # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_SPEED_EXACTLY=response.value
	    GET_SPEED=str_int(str(response.value))

	cmd = obd.commands.TIMING_ADVANCE # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_TIMING=str_int(str(response.value),1)
#2
	cmd = obd.commands.SHORT_FUEL_TRIM_1 # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_SHORT_L=str_int(str(response.value),1)

	cmd = obd.commands.RELATIVE_THROTTLE_POS # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_THROTTLE=str_int(str(response.value))

	cmd = obd.commands.THROTTLE_POS # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_THROTTLE_ABS=str_int(str(response.value))
#3
	cmd = obd.commands.LONG_FUEL_TRIM_1 # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_LONG_L=str_int(str(response.value),1)

	cmd = obd.commands.ENGINE_LOAD # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_LOAD=str_int(str(response.value))

	cmd = obd.commands.ABSOLUTE_LOAD # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_LOAD_ABS=str_int(str(response.value))
#4
	cmd = obd.commands.COOLANT_TEMP # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_TEMP=str_int(str(response.value))

	cmd = obd.commands.FUEL_STATUS # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_FUEL_STATUS=response.value

	cmd = obd.commands.FUEL_LEVEL # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	if response.value!=None:
	    GET_FUEL_LEVEL=str_int(str((response.value*62)/100))

	cmd = obd.commands.COMMAND_EQUIV_RATIO # select an OBD command (sensor)
	response = connection.query(cmd) # send the command, and parse the response
	print response.value
	if response.value!=None:
	    GET_AFR=str_int(str(14.7/response.value),2)



def print_values():
    while STOP_PRINT:

	print_rpm()
	print_speed()
	print_timing()

	print_short_l()
	print_throttle()
	print_throttle_abs()

	print_long_l()
	print_load()
	print_load_abs()

	print_temp()
	print_fuel_status()
	print_accel()

	global rotated, gif, my_count
	rotated = new_img
	gif = ImageTk.PhotoImage(rotated)  #This is PIL stuff, but if I put this statement before the statement: 
	tk_img.configure(image = gif)
	tk_img.image = gif
	time.sleep(0.2)
	#my_count=my_count+1

#first row
def print_rpm():
    pen.rectangle( (0,0,160,80), fill=(32,0,0))
    #print GET_RPM,"++"
    pen.text((25, 5), GET_RPM,  font=my_font, fill=(255,55,55))
    pen.text((25, 50), "engine rpm",  font=my_font_spell, fill=(255,55,55))

def print_speed():
    pen.rectangle( (160,0,320,80), fill=(22,0,15))
    pen.text((200, 5), GET_SPEED,  font=my_font, fill=(255,0,155))
    pen.text((190, 50), "speed km/h",  font=my_font_spell, fill=(255,0,155))

def print_timing():
    pen.rectangle((320,0,480,80), fill=(0,0,25))
    pen.text( (350, 5), GET_TIMING,  font=my_font, fill=(0,0,255))
    pen.text((340, 50), "timing adv. %",  font=my_font_spell, fill=(0,0,255))

#secont raw
def print_short_l():
    pen.rectangle( (0,80,160,160), fill=(0,22,0))
    pen.text((50, 80), GET_SHORT_L,  font=my_font, fill=(0,255,0))
    pen.text((2, 130), "fuel trim short%",  font=my_font_spell, fill=(0,255,0))

def print_throttle():
    pen.rectangle( (160,80,320,160), fill=(0,0,22))
    pen.text( (200, 80), GET_THROTTLE,  font=my_font, fill=(0,0,255))
    pen.text((190, 130), "throttle %",  font=my_font_spell, fill=(0,0,255))


def print_throttle_abs():
    pen.rectangle( (320,80,480,160), fill=(0,0,35))
    pen.text( (355, 80), GET_THROTTLE_ABS,  font=my_font, fill=(0,0,255))
    pen.text((330, 130), "throttle abs %",  font=my_font_spell, fill=(0,0,255))

#third row
def print_long_l():
    pen.rectangle( (0,160,160,240), fill=(0,35,0))
    pen.text( (55, 160), GET_LONG_L,  font=my_font, fill=(0,255,0))
    pen.text((5, 210), "fuel trim long%",  font=my_font_spell, fill=(0,255,0))

def print_load():
    pen.rectangle( (160,160,320,240), fill=(22,22,0))
    pen.text( (200, 155), GET_LOAD,  font=my_font, fill=(255,255,0))
    pen.text((190, 210), "calc loadc %",  font=my_font_spell, fill=(255,255,0))

def print_load_abs():
    pen.rectangle( (320,160,480,240), fill=(35,35,0))
    pen.text( (355, 155), GET_LOAD_ABS,  font=my_font, fill=(255,255,0))
    pen.text((350, 210), "abs load %",  font=my_font_spell, fill=(255,255,0))

#fourth row
def print_temp():
    pen.rectangle( (0,240,160,320), fill=(32,0,0))
    pen.text( (50, 240), GET_TEMP,  font=my_font, fill=(255,0,0))
    pen.text((10, 290), "temperature C",  font=my_font_spell, fill=(255,0,0))
    
def print_fuel_status():

    fuel_color=(255,255,255)
    pen.rectangle( (160,240,320,320), fill=(52,0,0))
#    pen.text( (165, 240),"Open loop acceleration",  font=my_font_spell, fill=fuel_color)

    if GET_FUEL_STATUS!=None:


	if GET_FUEL_STATUS=="Closed normal":
	    fuel_color=(55,255,55)

	if GET_FUEL_STATUS=="Open temp":
	    fuel_color=(55,55,255)

	if GET_FUEL_STATUS=="Open load":
	    fuel_color=(255,255,55)

	if GET_FUEL_STATUS=="Open failure" or GET_FUEL_STATUS=="Closed failure":
	    fuel_color=(255,55,55)

	pen.text( (165, 290), GET_FUEL_STATUS,  font=my_font_spell, fill=fuel_color)
#	pen.text( (165, 260), text[1],  font=my_font_spell, fill=fuel_color)
#	pen.text( (165, 280), text[2],  font=my_font_spell, fill=fuel_color)
    if GET_AFR!=None:
	    pen.text( (170, 240), GET_AFR,  font=my_font, fill=(255,255,55))
    pen.text( (290, 250), GET_FUEL_LEVEL+"l",  font=my_font_spell, fill=(255,255,255))

def print_accel():
	global ACCEL_X
	pen.rectangle( (320,240,480,260), fill=(0,0,0))
	pen.text( (330, 240), str(GET_ACCEL),  font=my_font_spell, fill=(0,255,0))
	#print ACCEL_X, GET_ACCEL
	if ACCEL_X>=479:
		pen.rectangle( (320,260,480,320), fill=(0,0,0))
		ACCEL_X=320
	if GET_ACCEL>0:
		ACCEL_X=ACCEL_X+1
		pen.line((ACCEL_X,320,ACCEL_X,320-GET_ACCEL),fill=(255,255,0))
		pen.line((ACCEL_X+1,320,ACCEL_X+1,320-GET_ACCEL),fill=(0,0,0))

def calcAccel():
	while STOP_ACCEL:
		global  GET_ACCEL
		v1=(GET_SPEED_EXACTLY*1000)/3600
		t1=time.time()
		time.sleep(0.2)
		v2=(GET_SPEED_EXACTLY*1000)/3600
		t2=time.time()
		GET_ACCEL=((v2-v1)/(t2-t1))*2000
		#GET_ACCEL=60
		#print GET_ACCEL



def quit(event=None):
    global STOP_GET,STOP_PRINT,STOP_ACCEL
    STOP_GET=0
    STOP_PRINT=0
    STOP_ACCEL=0
    #connection.close()
    #print "Quit",event
    #t.kill()
    sys.exit(0)



#-------PIL stuff:

new_img = Image.new("RGB", (480, 320), "black" )
pen = ImageDraw.Draw(new_img)

#my_font = ImageFont.load_default()
my_font = ImageFont.truetype("/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-B.ttf", 45)   #Look for files on your system with a .ttf extension 
my_font_spell = ImageFont.truetype("/usr/share/fonts/truetype/ubuntu-font-family/Ubuntu-B.ttf", 20)   #Look for files on your system with a .ttf extension 

#rotated = new_img.transpose(Image.FLIP_TOP_BOTTOM)
rotated = new_img


displayed_images = []    
root = tk.Tk()     
root.bind_all("<Button-1>", quit)

gif = ImageTk.PhotoImage(rotated)  #This is PIL stuff, but if I put this statement before the statement: 
                                   #root = tk.Tk(), then I get an error.
#displayed_images.append(gif)   #For some reason this is necessary to prevent
                               #the image from being garbage collected.
tk_img = tk.Label(image=gif)
tk_img.pack()

#connection=0

#while !connection:
connection = obd.OBD("/dev/ttyUSB0") # auto-connects to USB or RF port
#time.sleep(3)
#connection.close()
#    cmd = obd.commands.RPM # select an OBD command (sensor)
#    response = connection.query(cmd) # send the command, and parse the response
#    print(response.value)
#    print(response.unit)

Thread_print=threading.Thread(target=print_values)
Thread_print.daemon = False
Thread_print.start()

Thread_getValues=threading.Thread(target=getValues)
Thread_getValues.daemon = False
Thread_getValues.start()

Thread_calcAccel=threading.Thread(target=calcAccel)
Thread_calcAccel.daemon = False
Thread_calcAccel.start()


root.attributes('-fullscreen', True)
#root.geometry("480x320")             #Set the window dimensions
#root.bind('<KeyPress>', onKeyPress)

root.mainloop()
